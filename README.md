# Containerized PHPMyAdmin

Containerized PHPMyAdmin.

### Usage

`cd` into the root of the directory and execute the following command to star the containers:
```shell
docker compose up -d
```
The command will spin up two containers: phpMyAdmin and MySQL database.

To access the application type in the address bar of you internet browser the following address:
`http://localhost:8181`
The port number is defined in the `compose.yml` file and can be changed to any other port.
